FROM java:8
EXPOSE 8080
ADD /build/libs/dockeraws-0.0.1-SNAPSHOT.war docker.war
ENTRYPOINT ["java","-jar","docker.war"]
